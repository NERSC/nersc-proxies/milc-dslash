// SYCL implementation
#include <CL/sycl.hpp>
#define THREADS_PER_SITE 48

using namespace cl::sycl;

class make_back;
class dslash;

double dslash_fn(
    const std::vector<su3_vector> &src, 
          std::vector<su3_vector> &dst,
    const std::vector<su3_matrix> &fat,
    const std::vector<su3_matrix> &lng,
          std::vector<su3_matrix> &fatbck,
          std::vector<su3_matrix> &lngbck,
    size_t *fwd, size_t *bck, size_t *fwd3, size_t *bck3,    
    const size_t iterations,
    size_t wgsize )
{ 
  // Set device and queue
  default_selector selector;
  queue queue(selector);
  
  // Print device information
  std::cout << "Getting device info" << std::endl << std::flush;
  std::cout << "Device " << selector.select_device().get_info<info::device::name>() \
	    << ":Driver " << selector.select_device().get_info<info::device::driver_version>() << std::endl;
  std::cout << std::flush;

  // Pre-build the dslash kernel
  auto build_start = Clock::now();
  program program1 = program(queue.get_context());
  program1.build_with_kernel_type<dslash>();
  double build_time = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-build_start).count();
  std::cout << "Time to build kernel = " << build_time/1.0e6 << " secs\n";
  std::cout << std::flush;
  
  // Set the loop and work-group parameters
  if (wgsize == 0)
    wgsize = THREADS_PER_SITE;
  
  size_t total_sites = sites_on_node; 
  size_t total_even_sites = even_sites_on_node;

  std::cout << "max compute units = " 
	    << queue.get_device().get_info<info::device::max_compute_units>() << "\n";
  std::cout << "max workgroup size = " 
	    << queue.get_device().get_info<info::device::max_work_group_size>() << "\n";
  std::cout << std::flush;
  
  auto copy_start = Clock::now();
  // create SYCL buffers
  // Only src, fat and lng are moved to the device
  buffer<su3_vector, 1> b_src    {src.data(), range<1> {total_sites * 1}};
  buffer<su3_vector, 1> b_dst    {            range<1> {total_sites * 1}};
  buffer<su3_matrix, 1> b_fat    {fat.data(), range<1> {total_sites * 4}};
  buffer<su3_matrix, 1> b_lng    {lng.data(), range<1> {total_sites * 4}};
  buffer<su3_matrix, 1> b_fatbck {            range<1> {total_sites * 4}};
  buffer<su3_matrix, 1> b_lngbck {            range<1> {total_sites * 4}};

  // The resultant arrays aren't copied in the above buffer allocations,
  // so we need to tell the runtime to copy them back
  b_dst.set_final_data(dst.data());
  b_fatbck.set_final_data(fatbck.data());
  b_lngbck.set_final_data(lngbck.data());

  // create SYCL buffers for device gathers
  buffer<size_t, 1> b_fwd  {fwd,  range<1> {total_sites * 4}};
  buffer<size_t, 1> b_bck  {bck,  range<1> {total_sites * 4}};
  buffer<size_t, 1> b_fwd3 {fwd3, range<1> {total_sites * 4}};
  buffer<size_t, 1> b_bck3 {bck3, range<1> {total_sites * 4}};

  double copy_time = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-copy_start).count();
  std::cout << "Time to offload input data = " << copy_time/1.0e6 << " secs\n";
  std::cout << std::flush;

  // Create backward links on the device
  std::cout << "Creating backward links"  << std::endl;
  size_t total_wi = total_even_sites;
  std::cout << "Setting number of work items " << total_wi << std::endl;
  std::cout << "Setting workgroup size to " << 1 << std::endl;
  auto back_start = Clock::now();
  queue.submit(
    [&](handler& cgh) {
    auto d_fat = b_fat.get_access<access::mode::read>(cgh);
    auto d_lng = b_lng.get_access<access::mode::read>(cgh);
    auto d_fatbck = b_fatbck.get_access<access::mode::write>(cgh);
    auto d_lngbck = b_lngbck.get_access<access::mode::write>(cgh);
    auto d_bck = b_bck.get_access<access::mode::read>(cgh);
    auto d_bck3 = b_bck3.get_access<access::mode::read>(cgh);
    // Lambda function defines the kernel scope
    cgh.parallel_for<class make_back>(
    nd_range<1> {total_wi, 1}, [=](nd_item<1> item) {
      size_t myThread = item.get_global_id(0);
      size_t mySite = myThread;  // Do only even site
      if (mySite < total_even_sites) {
        for(int dir = 0; dir < 4; dir++) {
				  // since accessors are of type "read", must use a pointer to avoid implicit copy back
          const auto p_fat = d_fat.get_pointer();
          const auto p_lng = d_lng.get_pointer();
          su3_adjoint( p_fat + 4*d_bck[4*mySite+dir]+dir, &d_fatbck[0] + 4*mySite+dir );
          su3_adjoint( p_lng + 4*d_bck3[4*mySite+dir]+dir, &d_lngbck[0] + 4*mySite+dir );
        }
      }
    }); // end of the kernel lambda function
  });   // end of command group
  queue.wait();
  double back_time = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-back_start).count();
  std::cout << "Time to create back links = " << back_time/1.0e6 << " secs\n";
  std::cout << std::flush;

  // Dslash benchmark loop
  std::cout << "Running dslash loop" << std::endl;
  total_wi = total_even_sites * THREADS_PER_SITE;
  std::cout << "Setting number of work items to " << total_wi << std::endl;
  std::cout << "Setting workgroup size to " << wgsize << std::endl;
  auto tstart = Clock::now();
  for (int iters=0; iters<iterations+warmups; ++iters) {
    if (iters == warmups) {
      queue.wait();
      tstart = Clock::now();
    } 

      
    // Dslash kernel
    queue.submit([&](handler& cgh) {
      auto d_src = b_src.get_access<access::mode::read>(cgh);
      auto d_dst = b_dst.get_access<access::mode::write>(cgh);
      auto d_fat = b_fat.get_access<access::mode::read>(cgh);
      auto d_lng = b_lng.get_access<access::mode::read>(cgh);
      auto d_fatbck = b_fatbck.get_access<access::mode::read>(cgh);
      auto d_lngbck = b_lngbck.get_access<access::mode::read>(cgh);
      auto d_fwd = b_fwd.get_access<access::mode::read>(cgh);
      auto d_fwd3 = b_fwd3.get_access<access::mode::read>(cgh);
      auto d_bck = b_bck.get_access<access::mode::read>(cgh);
      auto d_bck3 = b_bck3.get_access<access::mode::read>(cgh);

      auto v = accessor<Complx, 1, access::mode::read_write, access::target::local>(wgsize, cgh);
      // Lambda function defines the kernel scope
      cgh.parallel_for<class dslash>(program1.get_kernel<dslash>(),
      nd_range<1> {total_wi, wgsize}, [=](nd_item<1> item) {
        size_t myThread = item.get_global_id(0);
        size_t mySite = myThread/THREADS_PER_SITE;
	
	//wgsize=48,96 -> must be a multiple of 48 and divisible by total_wi
#if OPTION == 1
        int k = myThread%4;
        int row = (myThread/4)%3;
#else
        int k = (myThread/3)%4;
        int row = myThread%3;
#endif
        int mat = (myThread/12)%4;

        auto local_id = item.get_local_id(0);

        if (mySite < total_even_sites) {
          // since accessors are of type "read", must use a pointer to avoid implicit copy back
          const auto p_src = d_src.get_pointer();
          const auto p_fat = d_fat.get_pointer();
          const auto p_lng = d_lng.get_pointer();
          const auto p_fatbck = d_fatbck.get_pointer();
          const auto p_lngbck = d_lngbck.get_pointer();

          if (mat == 0) {
              auto a = p_fat + mySite*4 + k;
              auto b = p_src + d_fwd[4*mySite + k];
              Complx x = {0.0, 0.0};
              for(int j=0;j<3;j++){
                CMULSUM( a->e[row][j] , b->c[j] , x );
              }
              v[local_id].real = x.real;
              v[local_id].imag = x.imag;
          }
          else if (mat == 1) {
              auto a = p_lng + mySite*4 + k;
      	      auto b = p_src + d_fwd3[4*mySite + k];
              Complx x = {0.0, 0.0};
              for(int j=0;j<3;j++){
                CMULSUM( a->e[row][j] , b->c[j] , x );
              }
              v[local_id].real = x.real;
              v[local_id].imag = x.imag;
          }
          else if (mat == 2) { 
              auto a = p_fatbck + mySite*4 + k;
      	      auto b = p_src + d_bck[4*mySite + k];
              Complx x = {0.0, 0.0};
              for(int j=0;j<3;j++){
                CMULSUM( a->e[row][j] , b->c[j] , x );
              }
              v[local_id].real = x.real;
              v[local_id].imag = x.imag;
          }
          else if (mat == 3) {
              auto a = p_lngbck + mySite*4 + k;
      	      auto b = p_src + d_bck3[4*mySite + k];
              Complx x = {0.0, 0.0};
              for(int j=0;j<3;j++){
                CMULSUM( a->e[row][j] , b->c[j] , x );
              }
              v[local_id].real = x.real;
              v[local_id].imag = x.imag;
          }

          item.barrier(sycl::access::fence_space::local_space);

          if( mat == 0) { 
              v[local_id].real = v[local_id].real + v[local_id+12].real - v[local_id+24].real - v[local_id+36].real;
              v[local_id].imag = v[local_id].imag + v[local_id+12].imag - v[local_id+24].imag - v[local_id+36].imag;
          }

          item.barrier(sycl::access::fence_space::local_space);

          if( mat == 0 && k == 0) { 
#if OPTION == 1
              d_dst[mySite].c[row].real = v[local_id].real + v[local_id+1].real + v[local_id+2].real + v[local_id+3].real;
              d_dst[mySite].c[row].imag = v[local_id].imag + v[local_id+1].imag + v[local_id+2].imag + v[local_id+3].imag;
#else
              d_dst[mySite].c[row].real = v[local_id].real + v[local_id+3].real + v[local_id+6].real + v[local_id+9].real;
              d_dst[mySite].c[row].imag = v[local_id].imag + v[local_id+3].imag + v[local_id+6].imag + v[local_id+9].imag;
#endif
          }

        } // if mySite
      }); // end of the kernel lambda function
    });   // end of command group
    queue.wait();
  } // end of iteration loop
  double ttotal = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-tstart).count();
    
  // SYCL runtime does the copy back at function exit
  
  return (ttotal /= 1.0e6);
}
