#!/bin/bash
set -x
for t in 4 8 16 32 64 128 256 512; do 
  command="srun dslash_dpcpp.x $t"
  echo $command
  $command
  echo ""
done
