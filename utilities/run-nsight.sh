#!/bin/bash
set -x

exe=./dslash_dpcpp.x
kernel=dslash
threads=128

filename=profile-${kernel}-t${threads}
skip=1
count=10
#sections="--section ComputeWorkloadAnalysis --section MemoryWorkloadAnalysis --section Occupancy --section SpeedOfLight --section SpeedOfLight_RooflineChart"
sections="--section SpeedOfLight --section SpeedOfLight_RooflineChart --section SchedulerStats --section WarpStateStats --section MemoryWorkloadAnalysis"

srun nv-nsight-cu-cli -o $filename -k $kernel -s $skip -c $count $sections $exe $threads

